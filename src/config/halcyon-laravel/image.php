<?php

return [

    'auth' => [
        'user' => [
            'model' => App\Models\Auth\User::class
        ]
    ],

    'migration' => [
        'table_name' => 'images',
    ],

    'valid_file_type' => [
        'image/png',
        'image/jpeg',
        'image/jpg',
    ],

    'folder_permission' => 0755,
    
    'prefix' => 'photo-',

    // 'max_file_size' => 2,
    'max_width' => 1024,
    'max_height' => 1024,

    'quality' => 90,

    /** please do not modify this, this will fix soon, ask lloric about it */
    'is_storage' => true,

    'can_delete' => true,
    
];

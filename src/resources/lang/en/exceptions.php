<?php

return [
        'invalid_file_type' => 'Wrong file mime type.',
        'required_file_type' => 'Please indicate width and height for this file size.',
        'failed_delete' => 'Failed or invalid to delete files.'
];

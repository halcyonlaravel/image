<?php

namespace HalcyonLaravel\Image\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Response;

class ValidationException extends HttpException
{
    public static function required(): self
    {
        return new static(Response::HTTP_BAD_REQUEST, trans('image::exceptions.required_file_type'), null, []);
    }
    
    public static function invalidFileType(): self
    {
        return new static(Response::HTTP_BAD_REQUEST, trans('image::exceptions.invalid_file_type'), null, []);
    }
}

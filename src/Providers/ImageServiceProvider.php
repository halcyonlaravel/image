<?php
namespace HalcyonLaravel\Image\Providers;

use Illuminate\Support\ServiceProvider;

// use HalcyonLaravel\Image\Console\ImageTableCommand;

class ImageServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        if (! preg_match('/lumen/i', app()->version())) {

            // Publish Image Config
            $this->publishes([ __DIR__.'/../config/halcyon-laravel/image.php' => config_path('halcyon-laravel/image.php'), ]);

            if (! class_exists('CreatePermissionTables')) {
                $timestamp = date('Y_m_d_His', time());
                $this->publishes([
                    __DIR__.'/../../database/migrations/migration.stub' => $this->app->databasePath()."/migrations/{$timestamp}_create_images_table.php",
                ], 'migrations');
            }
        }


        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'image');

        $this->loadRoutesFrom(__DIR__.'/../Http/route.php');
    }

    /**
    * Register the application services.
    *
    * @return void
    */
    public function register()
    {
        // $this->commands([
        //     ImageTableCommand::class,
        // ]);

        // Merge Image Config
        $this->mergeConfigFrom(__DIR__.'/../config/halcyon-laravel/image.php', 'halcyon-laravel.image');
    }
}

<?php


namespace HalcyonLaravel\Image\Models\Traits;

use HalcyonLaravel\Image\Models\Image as Model;
use HalcyonLaravel\Image\Models\Helpers\ImageUploader;
use HalcyonLaravel\Image\Models\Helpers\ImageGetter;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait ImageTrait
{
    /**
     * Return Images relationship
     *
     * @return Builder
     */
    public function images() :MorphMany
    {
        return $this->morphMany(Model::class, 'imageable')->orderBy('order');
    }

    /**
     *
     * Uploads the images to the server and generates registered thumbnails for the model.
     * @param array $files
     *
     * @return array|null $filenames
     * @throws Exception $e
     */
    public function uploadImages(array $files = null, $category = null, $includeOriginal = true) :array
    {
        if (is_null($files)) {
            return [];
        }

        return (new ImageUploader($this, $files))->category($category)->includeOriginal($includeOriginal)
            ->uploadImages();
    }

    /**
     *
     * Return the images on a specific size
     * @param int $widthOrName
     * @param int $width
     * @param string $category
     *
     * @throws Exception $e
     */
    public function getImages($widthOrName = null, $height = null, $category = null) :MorphMany
    {
        if (!is_null($widthOrName) && is_null($height) && is_string($widthOrName)) {
            return (new ImageGetter($this))
                ->getImagesByName($widthOrName, $category);
        }
        
        return (new ImageGetter($this))
            ->getImages($widthOrName, $height, $category);
    }

    public function deleteAllImages($category = null)
    {
        return Model::deleteFiles([
            'id' => $this->id,
            'type' => self::class,
        ], 'model', $category);
    }
}

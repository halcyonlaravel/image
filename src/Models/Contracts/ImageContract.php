<?php

namespace HalcyonLaravel\Image\Models\Contracts;

interface ImageContract
{
    /**
     * Return the array of sizes when uploading a specific image
     * @return array
     */
    public static function image_sizes() : array;
}

<?php

namespace HalcyonLaravel\Image\Models\Helpers;

use Illuminate\Database\Eloquent\Model;
use HalcyonLaravel\Image\Exceptions\ValidationException;
use HalcyonLaravel\Image\Models\Image as ImageModel;
use File;
use Spatie\Image\Image as ImageSpatie;
use Spatie\Image\Manipulations;

class ImageUploader
{
    /**
     *
     */
    private $_laravelPath;
    
    /**
     * instance
     */
    private $_file;

    /**
     * array of Files
     */
    private $_files;

    /**
     * Directory to upload
     */
    private $_path;

    /**
     * Images's model
     */
    private $_model;

    /**
     * Current datetime for loop iteration
     */
    private $_now;

    /**
     * Image's category
     */
    private $_category;

    private $_includeOriginal;

    /**
    *
    * Class constructor
    * @param Illuminate\Database\Eloquent\Model $model
    * @param array $files
    */
    public function __construct(Model $model, array $files)
    {
        $this->_model = $model;
        $this->_files = $files;
        $this->_laravelPath = config('halcyon-laravel.image.is_storage') ? storage_path('app/public') : public_path();
    }

    /**
     * Set the current file being uploaded
     *
     *
     */
    private function _setFile($file)
    {
        $this->_file = $file;
    }

    /**
     * Set the current category to be uploaded
     *
     *
     */
    public function category($category)
    {
        $this->_category = $category;

        return $this;
    }
    public function includeOriginal(bool $includeOriginal)
    {
        $this->_includeOriginal = $includeOriginal;
        return $this;
    }
    
    /**
     *
     * Uploads the images to the server and generates registered thumbnails for the model.
     * @param array $files
     *
     * @return array $filenames
     * @throws Exception $e
     */
    public function uploadImages() :array
    {
        $moduleName = explode('\\', get_class($this->_model));
        $folderName = 'assets/'.strtolower(end($moduleName));
        // $this->_path = "$this->_laravelPath/$folderName/{$this->_model->id}";
        $this->_path = "$this->_laravelPath/$folderName/" . md5(now()->today() . $this->_model->id);

        // Create folder if not existing
        if (! file_exists($this->_path)) {
            File::makeDirectory($this->_path, config('halcyon-laravel.image.folder_permission'), true, true);
        }

        $hisgestOrder = $this->_getHisgestOrder();

        $filenames = [];
        $this->_now = now();
        foreach ($this->_files as $k => $file) {
            if (is_file($file)) {
                $this->_setFile($file);
                $filenames[] = $this->_process($hisgestOrder++);
                $this->_now = $this->_now->addSeconds(1);
            }
        }

        return $filenames;
    }

    private function _getHisgestOrder() :int
    {
        $image = $this->_model->images()->select('order')->orderBy('order', 'desc')->first();
        return (is_null($image)?0:$image->order)+1;
    }

    /**
     *
     */
    private function _process(int $orderCount) : array
    {
        $type  = $this->_file->getMimeType();
        
        // Check for image type
        if (! in_array($type, config('halcyon-laravel.image.valid_file_type'))) {
            throw ValidationException::invalidFileType();
        }

        // store data for database insert.
        $filenames = [];

        // for 'created_at'
        $now = $this->_now->format('Y-m-d h:i:s');
        
        if ($this->_category) {
            $sizes = $this->_model::image_sizes()[$this->_category];
        } else {
            $sizes = $this->_model::image_sizes();
        }

        foreach ($sizes as $s => $size) {

            // Save resizing Image
            $upload = $this->_uploadingImage($size);

            $className = get_class($this->_model);

            // append data for database.
            $filenames[] = [
                'name' => isset($size['name']) ? $size['name'] : strtolower(str_replace('\\', '-', $className)) . '-' . ($s + 1),
                'filename' => str_replace($this->_laravelPath, '', "$this->_path/{$upload['name']}"),
                'category' => $this->_category,
                'user_id' => auth()->id()?:1,
                'imageable_type' => $className,
                'imageable_id' => $this->_model->id,
                'type' => $upload['sizeStr'],
                'order' => $orderCount,
                'group' => $upload['group'],
                'created_at' => $now ,
            ];
        }

        if ($this->_includeOriginal) {
            $originalImage = $this->_uploadingImage();

            // append data for database.
            $filenames[] = [
                // Save Original Image
                'name' => 'original',
                'filename' => str_replace($this->_laravelPath, '', "$this->_path/{$originalImage['name']}"),
                'category' => $this->_category,
                'user_id' => auth()->id()?:1,
                'imageable_type' => get_class($this->_model),
                'imageable_id' => $this->_model->id,
                'type' => null,
                'order' => $orderCount,
                'group' => $originalImage['group'],
                'created_at' => $now,

            ];
        }
        /**
         * Saving Data into Database.
         */
        $this->_model->images()->insert($filenames);

        return $filenames;
    }

    /**
     *
     */
    private function _uploadingImage(array $size = null) :array
    {
        $dimension = null;
        $sizeStr = null;
        $crop = Manipulations::FIT_CONTAIN;
        // $group = $name = md5(get_class($this->_model) . $this->_model->id . $this->_now->format('Ymdhis') . $this->_category). '.' . $this->_file->getClientOriginalExtension();
        $group = md5(get_class($this->_model) . $this->_model->id . $this->_now->format('Ymdhis') . $this->_category);
        $name = $group . '.' . $this->_file->getClientOriginalExtension();

        if (is_null($size)) {
            $dimension = $this->_fileDimensions();
        } else {
            $dimension = $this->_fileDimensions($size);

            $sizeStr = "{$size['width']}x{$size['height']}";
            $name = config('halcyon-laravel.image.prefix') . "{$sizeStr}-{$name}";
            if (!empty($size['crop'])) {
                if ($size['crop'] === true) {
                    $crop = Manipulations::FIT_CROP;
                } elseif ($size['crop'] === false) {
                    $crop = Manipulations::FIT_CONTAIN;
                } else {
                    $crop = $crop;
                }
            }
        }

        $_width = $dimension[0];
        $_height = $dimension[1];

        // Actual Upload Image
        ImageSpatie::load($this->_file)
            ->optimize()
            ->fit($crop, $_width, $_height)
            ->quality(config('halcyon-laravel.image.quality'))
            ->save($this->_path.'/'.$name);
        
        // if ($crop) {
        //     $image->fit($_width, $_height);
        // } else {
        //     $image->resize($_width, $_height, function ($crop) {
        //         $crop->aspectRatio();
        //     });
        // }

        return [
            'name' => $name,
            'sizeStr' => $sizeStr,
            'group' => $group,
        ];
    }

    /**
     *
     */
    private function _fileDimensions($dimension = null)
    {
        if (is_null($dimension)) {
            $dimensions = getimagesize($this->_file);
            $width  = $dimensions[0] > config('halcyon-laravel.image.max_width') ? config('halcyon-laravel.image.max_width') : $dimensions[0];
            $height = $dimensions[1] > config('halcyon-laravel.image.max_height') ? config('halcyon-laravel.image.max_height') : $dimensions[1];
        } else {
            if (! array_key_exists('width', $dimension) || ! array_key_exists('height', $dimension)) {
                throw ValidationException::required();
            }
            $width  = $dimension['width'] > config('halcyon-laravel.image.max_width') ? config('halcyon-laravel.image.max_width') : $dimension['width'];
            $height = $dimension['height'] > config('halcyon-laravel.image.max_height') ? config('halcyon-laravel.image.max_height') : $dimension['height'];
        }
        return [
            $width, $height
       ];
    }
}

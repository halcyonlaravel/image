<?php


namespace HalcyonLaravel\Image\Models\Helpers;

use HalcyonLaravel\Image\Exceptions\Validation;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Model;
use Image;
use File;

class ImageGetter
{
    private $_model;
    /**
     * ImageGetter Constructor
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->_model = $model;
    }

    /**
     *
     * @param string $dimension wxh
     * @param string $category
     */
    public function getImages($width = null, $height = null, $category = null) : MorphMany
    {
        $query = $this->_model->images()->where('category', $category);
        if (is_null($width) || is_null($width)) {
            $query->whereNull('type');
        } else {
            $query->where('type', "{$width}x{$height}");
        }
        return $query;
    }

    /**
     *
     * @param string $name
     * @param string $category
     */
    public function getImagesByName(string $name, $category = null) : MorphMany
    {
        return $this->_model->images()
            ->where('category', $category)
            ->where('name', $name);
    }
}

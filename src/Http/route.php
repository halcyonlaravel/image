<?php

Route::group([
    'namespace'  => 'HalcyonLaravel\Image\Http\Controllers',
], function () {
    Route::get('image/{path}', 'ImagesController@show')->where('path', '.+')->name('image.show');
    Route::delete('image/{path}', 'ImagesController@destroy')->where('path', '.+')->name('image.destroy');
});

<?php

namespace HalcyonLaravel\Image\Http\Controllers;

use Illuminate\Http\Request;
use HalcyonLaravel\Image\Models\Image as Model;
use Storage;

class ImagesController extends Controller
{
    // protected $model;

    // public function __construct(Model $model)
    // {
    //     $this->model = $model;
    // }

    public function show(string $path)
    {
        if (!Storage::exists($path)) {
            abort(404);
        }
        $file = Storage::get($path);
        $type = Storage::mimeType($path);
        return response($file)->header('Content-Type', $type);
    }


    public function destroy(Request $request, string $path)
    {
        return Model::deleteFiles($path);
    }
}

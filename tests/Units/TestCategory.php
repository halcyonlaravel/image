<?php

namespace HalcyonLaravel\Image\Tests\Units;

use HalcyonLaravel\Image\Tests\TestCase;
use Illuminate\Http\UploadedFile;
use HalcyonLaravel\Image\Models\Image as Model;
use App\Models\Sample;
use Storage;

class TestCategory extends TestCase
{
    public function testCategoryUpload()
    {

        $this->actingAs($this->user);

        $image = UploadedFile::fake()->image('avatar.jpg');
        $files = $this->sampleModelCategory->uploadImages([$image], 'category1');


        //  test getOtherImage(){};

        $images = $this->sampleModelCategory->getImages('large', null, 'category1')->get();
        $this->assertTrue(count($images) !== 0);

        // foreach($images as $file)
        // {
            foreach($images as $image)
            {
                Storage::disk('local')
                    ->assertExists("public{$image->filename_original}");

                    $mediumFileImgSrc =    $image->getOtherImage('medium')->first()->filename;

                    $this->json('GET', $mediumFileImgSrc)
                        ->assertOk();

                    $mediumFileImgSrc =    $image->getOtherImage(1024, 300)->first()->filename;

                    $this->json('GET', $mediumFileImgSrc)
                        ->assertOk();

                    $mediumFileImgSrc =    $image->getOtherImage()->first()->filename;

                    $this->json('GET', $mediumFileImgSrc)
                        ->assertOk();
            }
        // }


        $data = [
            'imageable_id' => $this->sampleModelCategory->id,
            'imageable_type' => get_class($this->sampleModelCategory),
            'user_id' => $this->user->id,
        ];

        foreach($files as $file)
        {
            foreach($file as $image)
            {
                Storage::disk('local')
                    ->assertExists("public{$image['filename']}");
                
                $this->assertDatabaseHas((new Model)->getTable(), array_merge($data, [
                    'filename' => $image['filename'],
                ]));
            }
        }

        $response = $this->sampleModelCategory->deleteAllImages('category1');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('{"message":"The files has been deleted."}', $response->getContent());

        foreach($files as $file)
        {
            foreach($file as $image)
            {
                Storage::disk('local')
                    ->assertMissing("public{$image['filename']}");

                $this->assertDatabaseMissing((new Model)->getTable(), array_merge($data, [
                    'filename' => $image['filename'],
                ]));
            }
        }
    }
}

<?php

namespace HalcyonLaravel\Image\Tests\Units;

use HalcyonLaravel\Image\Tests\TestCase;
use Illuminate\Http\UploadedFile;
use HalcyonLaravel\Image\Models\Image;
use App\Models\Sample;
use Symfony\Component\HttpFoundation\Response;

class TestExceptionFeature extends TestCase
{
    public function testRequired()
    {
        
        $this->actingAs($this->user);

        $response = $this->json('POST', route('test.upload2'), [
            'image' => UploadedFile::fake()->image('avatar.jpg')
        ]);
        // dd('ss');
        // dd($response);
        $response
        ->assertStatus(Response::HTTP_BAD_REQUEST)
        ->assertExactJson([
                'message'=>'Please indicate width and height for this file size.',
                // 'exception:'=>[
                //     'message'=>'not fooo',
                // ]
        ]);


    }    
    
    public function testInvalidFileType()
    {
        
        $this->actingAs($this->user);

        $response = $this->json('POST', route('test.upload2'), [
            'image' => UploadedFile::fake()->image('avatar.text')
        ]);
        // dd('ss');
        // dd($response);
        $response
        ->assertStatus(Response::HTTP_BAD_REQUEST)
        ->assertExactJson([
                'message'=>'Wrong file mime type.',
                // 'exception:'=>[
                //     'message'=>'not fooo',
                // ]
        ]);


    }

    public function testNoImage()
    {
        $this->assertEquals([], $this->sampleModel->uploadImages());
    }

    // public function testFailedDestroy()
    // {

    //     $image = UploadedFile::fake()->image('avatar.jpg');
    //     $this->sampleModel->uploadImages([$image]);

    //     $images = $this->sampleModel->getImages('image1')->get();

    //     foreach($images as $image)
    //     {
    //         $this->json('DELETE', $image->filename.'kkkk')
    //             ->assertStatus(Response::HTTP_BAD_REQUEST)
    //             ->assertExactJson([
    //                 'message' => 'Failed or invalid to delete files.',
    //             ]);
    //     }
    // }
}

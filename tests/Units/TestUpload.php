<?php

namespace HalcyonLaravel\Image\Tests\Units;

use HalcyonLaravel\Image\Tests\TestCase;
use Illuminate\Http\UploadedFile;
use HalcyonLaravel\Image\Models\Image as Model;
use App\Models\Sample;
use Storage;

class TestUpload extends TestCase
{
    public function testImageUploadStorage()
    {
        $this->actingAs($this->user);

        $image = UploadedFile::fake()->image('avatar.jpg');
        $files = $this->sampleModel->uploadImages([$image]);


        $data = [
            'imageable_id' => $this->sampleModel->id,
            'imageable_type' => get_class($this->sampleModel),
            'user_id' => $this->user->id,
        ];

        foreach($files as $file)
        {
            foreach($file as $image)
            {
                Storage::disk('local')
                    ->assertExists("public{$image['filename']}");
                
                $this->assertDatabaseHas((new Model)->getTable(), array_merge($data, [
                    'filename' => $image['filename'],
                ]));
            }
        }

        $response = $this->sampleModel->deleteAllImages();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('{"message":"The files has been deleted."}', $response->getContent());

        foreach($files as $file)
        {
            foreach($file as $image)
            {
                Storage::disk('local')
                    ->assertMissing("public{$image['filename']}");

                $this->assertDatabaseMissing((new Model)->getTable(), array_merge($data, [
                    'filename' => $image['filename'],
                ]));
            }
        }
    }
}

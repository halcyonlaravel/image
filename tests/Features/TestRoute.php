<?php

namespace HalcyonLaravel\Image\Tests\Feature;

use HalcyonLaravel\Image\Tests\TestCase;
use Illuminate\Http\UploadedFile;
use HalcyonLaravel\Image\Models\Image;
use App\Models\Sample;

class TestRoute extends TestCase
{


    public function setUp()
    {
        parent::setUp();

        $image = UploadedFile::fake()->image('avatar.jpg');
        $this->sampleModel->uploadImages([$image]);
    }

    public function testRouteShowSize()
    {
        $this->actingAs($this->user);
        // by size
        $images = $this->sampleModel->getImages(123, 456)->get();
        $this->assertTrue(count($images) > 0);
        

        foreach($images as $image)
        {
            
            $this->assertTrue($this->user instanceof $image->user);
            $this->assertEquals($this->user->id, $image->user->id);

           $this->json('GET', $image->filename)
           ->assertStatus(200);
        }
    }

    public function testRouteShowSizeName()
    {
        // by size name
        $images = $this->sampleModel->getImages('image1')->get();
        $this->assertEquals(1, count($images));

        foreach($images as $image)
        {
           $this->json('GET', $image->filename)
           ->assertStatus(200);
        }
    }

    public function testRouteShowSizeOriginal()
    {
        // by original
        $images = $this->sampleModel->getImages('original')->get();
        $this->assertEquals(1, count($images));

        foreach($images as $image)
        {
           $this->json('GET', $image->filename)
           ->assertStatus(200);
        }
    }

    public function testRouteShowSizeNullGetOriginal()
    {
        // by original
        $images = $this->sampleModel->getImages()->get();
        $this->assertEquals(1, count($images));

        foreach($images as $image)
        {
            $this->assertEquals('original', $image->name);
           $this->json('GET', $image->filename)
           ->assertStatus(200);
        }
    }

    public function test404()
    {

        $images = $this->sampleModel->getImages('image1')->get();

        foreach($images as $image)
        {
           $this->json('GET', $image->filename.'xxx')
           ->assertStatus(404);
        }
    }

    public function testDestroy()
    {

        $images = $this->sampleModel->getImages('image1')->get();

        foreach($images as $image)
        {
            $this->json('DELETE', $image->filename)
                ->assertOk()
                ->assertExactJson([
                    'message' => 'The files has been deleted.',
                ]);
        }
    }
}

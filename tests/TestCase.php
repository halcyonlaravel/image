<?php

namespace HalcyonLaravel\Image\Tests;

use Illuminate\Database\Schema\Blueprint;
use App\Models\User;
use Orchestra\Testbench\TestCase as Orchestra;
use App\Models\Sample;
use App\Models\SampleCategory;
use Route;
use File;

class TestCase extends Orchestra
{
    protected $user;
    protected $sampleModel;
    protected $sampleModelCategory;

    public function setUp()
    {
        parent::setUp();
        $this->setConfig();
        $this->setUpDatabase($this->app);
        $this->setUpSeed();
        Route::post('/test-update', 'App\Http\Controllers\SampleController')->name('test.upload');
        Route::post('/test2-update', 'App\Http\Controllers\SampleTwoController')->name('test.upload2');
    }
   

    public function tearDown()
    {
        if(File::exists(storage_path('app/public')))
        {
            $this->assertTrue(File::deleteDirectory(storage_path('app/public')));
        }

        if(File::exists(public_path()))
        {
            $this->assertTrue(File::deleteDirectory(public_path()));
        }
        
        parent::tearDown();

    }

    protected function setUpSeed()
    {
        $this->user = User::create([
            'first_name' => 'Basic',
            'last_name' => 'User',
        ]);

        $this->sampleModel = Sample::create([
            'name' => 'Test Value',
    ]);

    $this->sampleModelCategory = SampleCategory::create([
        'name' => 'Test Value',
    ]);

    }


    protected function setConfig(array $config = null)
    {
        $default = [
            'auth' =>[
                'user' =>[
                    'model' =>  \App\Models\User::class,
                    ]
                ],
            
                'valid_file_type' => [
                    'image/png',
                    'image/jpeg',
                    'image/jpg',
                ],

                'folder_permission' => 0755,

                'prefix' => 'thumb_',
                // 'max_file_size' => 2,
                'max_width' => 1024,
                'max_height' => 1024,
                'is_storage' => true,
                'can_delete' => true,

                'quality' => 90,
            ];
            
        $this->app['config']['halcyon-laravel.image'] = is_null($config) 
            ? $default 
            : array_replace($default, $config);
    }

    /**
     * Set up the database.
     *
     * @param \Illuminate\Foundation\Application $app
     */
    protected function setUpDatabase($app)
    {
        include_once __DIR__.'/../database/migrations/migration.stub';
        (new \CreateImagesTable())->up();


        $app['db']->connection()->getSchemaBuilder()->create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->timestamps();
        });
        // $app['db']->connection()->getSchemaBuilder()->create('images', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->integer('imageable_id');
        //     $table->string('imageable_type');
        //     $table->integer('user_id');
        //     $table->string('type')->nullable();
        //     $table->text('filename');
        //     $table->timestamp('created_at');
        // });
        $app['db']->connection()->getSchemaBuilder()->create('sample_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        $app['db']->connection()->getSchemaBuilder()->create('samples', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        $app['db']->connection()->getSchemaBuilder()->create('sample_two', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
    }

    // /**
    //  * Resolve application HTTP Kernel implementation.
    //  *
    //  * @param  \Illuminate\Foundation\Application  $app
    //  * @return void
    //  */
    // protected function resolveApplicationHttpKernel($app)
    // {
    //     $app->singleton('Illuminate\Contracts\Http\Kernel', 'App\Http\Kernel');
    // }



    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);
    }


    protected function getPackageAliases($app)
    {
        return [
        ];
    }

    protected function getPackageProviders($app)
    {
        return [
            "HalcyonLaravel\\Image\\Providers\\ImageServiceProvider",
            // ---
        ];
    }



}

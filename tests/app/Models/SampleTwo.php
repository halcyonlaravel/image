<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use HalcyonLaravel\Image\Models\Traits\ImageTrait;
use HalcyonLaravel\Image\Models\Contracts\ImageContract;
use  HalcyonLaravel\Image\Models\Image;

class SampleTwo extends Model implements ImageContract
{
    use ImageTrait;
    protected $table = 'sample_two';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];



    /**
	 * Return the array of sizes when uploading a specific image
	 * @return array 
	 */
    public static function image_sizes() : array
    {
        return [
            [
              
            ],
            // [
            //     'width' => 700,
            //     'height' => 500,
            // ],
        ];
    }

 
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use HalcyonLaravel\Image\Models\Traits\ImageTrait;
use HalcyonLaravel\Image\Models\Contracts\ImageContract;
use  HalcyonLaravel\Image\Models\Image;

class Sample extends Model implements ImageContract
{
    use ImageTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];



    /**
	 * Return the array of sizes when uploading a specific image
	 * @return array 
	 */
    public static function image_sizes() : array
    {
        return [
            [
                'name' => 'image1',
                'width' => 123,
                'height' => 456,
                'crop' => true
            ],
            // [
            //     'width' => 700,
            //     'height' => 500,
            // ],
        ];
    }

 
}

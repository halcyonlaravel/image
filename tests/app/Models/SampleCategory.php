<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use HalcyonLaravel\Image\Models\Traits\ImageTrait;
use HalcyonLaravel\Image\Models\Contracts\ImageContract;
use  HalcyonLaravel\Image\Models\Image;

class SampleCategory extends Model implements ImageContract
{
    use ImageTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];



    /**
	 * Return the array of sizes when uploading a specific image
	 * @return array 
	 */
    public static function image_sizes() : array
    {
        return [
            'category1' => [
                [ 'name' => 'large', 'width' => 1024, 'height' => 700 , 'crop' => false],
                [ 'name' => 'medium', 'width' => 1024, 'height' => 300 , 'crop' => false],
                [ 'name' => 'thumbnail', 'width' => 100, 'height' => 100, 'crop' => true ],
            ],
            'category2' => [
                [ 'name' => 'large', 'width' => 1170, 'height' => 600, 'crop' => true ],
                [ 'name' => 'medium', 'width' => 800, 'height' => 410, 'crop' => true ],
                [ 'name' => 'thumbnail', 'width' => 100, 'height' => 100, 'crop' => true ],
            ]
                ];
    }

 
}

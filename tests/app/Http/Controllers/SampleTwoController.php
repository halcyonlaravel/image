<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SampleTwo;

class SampleTwoController extends Controller
{
        public function __invoke(Request $request)
        {
                $model = SampleTwo::create([
                        'name' => 'Test Value',
                ]);
// dd($model->uploadImages([$request->image]));
               

                session()->flash('uploaded2', $model->uploadImages([$request->image]));
        }
}
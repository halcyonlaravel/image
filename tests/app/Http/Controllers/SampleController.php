<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sample;

class SampleController extends Controller
{
        public function __invoke(Request $request)
        {
                $model = Sample::create([
                        'name' => 'Test Value',
                ]);
// dd($model->uploadImages([$request->image]));
               

                session()->flash('uploaded', $model->uploadImages([$request->image]));
        }
}
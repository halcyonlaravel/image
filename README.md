# Halcyon Packages Images - 1.0.29


## Changes Log
- v1.0.30 Fix cropping issue
- v1.0.29 add table name then fix cro
- v1.0.28 Manipulation
- v1.0.27 Fix Group
- v1.0.26 Replace library Intervention to Spatie Image
- v1.0.25 Optomize, using spatie laravel-image-optiomizer
- v1.0.24 Optomize, (first attemp)
- v1.0.23 Revert
- v1.0.22 Add ordering
- v1.0.21 Add dynamicall exclude image original
- v1.0.20 Add configuration for image quality
- v1.0.19 Fix image group uniqueness
- v1.0.18 add test for get category
- v1.0.16 image_sizes set to static, enhance unit test
- v1.0.14 Fix delete function for file pattern method
- v1.0.13 Fix group query for selecting other image
- v1.0.13 Fix group query for selecting other image
- v1.0.12 Add optional categorization
- v1.0.11 Fix helper function for getting other image
- v1.0.10 add helper function to get other image by size 
- v1.0.9: add image upload option for force crop resizing.
- v1.0.8: additional delete function for manual delete.
- v1.0.7: additional name.
- v1.0.6: Allow null, then skip upload.
- v1.0.5: Fix deleting by group.
- v1.0.4: Hide data.
- v1.0.3: Fix for seeder, then remove file extension.
- v1.0.2: Fix delete files when delete model.
- v1.0.1: Fix readning publish config.
